import Hapi from '@hapi/hapi'
import * as Routes from './routes'

export const server = Hapi.server({
  port: 3000,
  host: '0.0.0.0'
})
console.log(Routes.rootRoute)
Object.values(Routes).forEach(route => server.route(route))

const startServer = async () => {
  await server.start()
  const logMessage = `Server started at ${server.settings.host}:${server.settings.port}`
  console.log(logMessage)
  server.log(logMessage)
}

startServer()
