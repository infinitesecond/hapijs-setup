import {
  getHelloValidator,
  getHelloController,
  getHelloNameValidator,
  getHelloNameController,
  createHelloValidator,
  createHelloController
} from './controllers'
const PATH_ROOT = 'hello'

export const helloRoute = {
  method: 'GET',
  path: `/${PATH_ROOT}`,
  handler: getHelloController,
  options: {
    validate: getHelloValidator
  }
}

export const helloNameRoute = {
  method: 'GET',
  path: `/${PATH_ROOT}/{name}`,
  handler: getHelloNameController,
  options: {
    validate: getHelloNameValidator
  }
}

export const createHelloRoute = {
  method: 'POST',
  path: `/${PATH_ROOT}`,
  handler: createHelloController,
  options: {
    validate: createHelloValidator
  }
}
